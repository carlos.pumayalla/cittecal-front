package com.citeccal.commons;

import java.io.Serializable;
import java.util.List;

import com.citeccal.models.FichaTecnica;

public interface GenericServiceAPI<T, ID extends Serializable> {
	
	T save(T entity);
		
	void delete(ID id);
	
	T get(ID id);
	
	List<T> getAll();


}

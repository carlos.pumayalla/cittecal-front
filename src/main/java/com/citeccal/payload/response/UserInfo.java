package com.citeccal.payload.response;

public class UserInfo {
	
	private Object roles;
	
	public Object getRoles() {
		return roles;
	}

	public void setRoles(Object roles) {
		this.roles = roles;
	}
}

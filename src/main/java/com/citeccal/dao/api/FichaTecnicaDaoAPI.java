package com.citeccal.dao.api;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.citeccal.models.FichaTecnica;

@Repository
public interface FichaTecnicaDaoAPI extends CrudRepository<FichaTecnica, Long> {
	
 public abstract FichaTecnica findByFichaId(String fichaId);
}